// vrsion 1.0.1
mmcore.IntegrationFactory.register(
    'Google Universal', {
        defaults: {
            dimensionName: 'dimension',
            modifiers: {
                sitePersist: function (data) {
                    /**
                     * modifier configuration
                     * Please adapt to match your case
                     */
                    var config = {
                        // allowed amount of characters to be sent to 3d party
                        maxDataSize: 150,
                        // cookie name for campaign data storage
                        cookieName: 'mm-if-site-persist-ua-' + data.dimensionIndex
                    },

                    getCampaignIndex=function(a){var b;for(b=a.length;b--;)if(new RegExp("("+data.campaign+")=").test(a[b]))return b;return-1},concatCampaigns=function(){var a=getFromStorage(config.cookieName),b=a?JSON.parse(a):[],c=getCampaignIndex(b);return-1!==c?b[c]=data.campaignInfo:b.push(data.campaignInfo),b.join("&")},getFromStorage=function(){return mmcore.GetCookie(config.cookieName,1)},saveToStorage=function(a){var b=a.split("&");mmcore.SetCookie(config.cookieName,JSON.stringify(b),365,1)},removeOldestCampaigns=function(a){for(var b=a.split("&");;){if(!(b.join("&").length>=config.maxDataSize))return b.join("&");b.shift()}},updateCampaignInfo=function(a){data.currentCampaignInfo=data.campaignInfo,data.campaignInfo=a},initialize=function(){var a=concatCampaigns();a=removeOldestCampaigns(a),updateCampaignInfo(a),saveToStorage(a)}();
                }
            },

            isStopOnDocEnd: false
        },
        validate: function (data) {
            if(!data.campaign)
                return 'No campaign.';

            if(data.dimensionIndex < 1 || data.dimensionIndex > 200 || isNaN(data.dimensionIndex))
                return 'Invalid dimension slot. Must be 1-200.';

            return true;
        },

        check: function (data) {
            var ga = window.ga || window[data.gaVariable] || window[window.GoogleAnalyticsObject];
            return ga && typeof ga.getAll === 'function';
        },

        exec: function (data) {
            var prodSand = data.isProduction ? 'MM_Prod' : 'MM_Sand',
                dimensionName = data.dimensionName,
                dimensionIndex = data.dimensionIndex,
                ga = window[data.gaVariable || window.GoogleAnalyticsObject || 'ga'],
                namespace = '';

            if (data.account) {
                namespace = 'mm_' + data.account.replace(/\W/g,'');
                ga('create', data.account, 'auto', {'name': namespace});
                ga(namespace + '.set', dimensionName + dimensionIndex, data.campaignInfo);
                ga(namespace + '.send', 'event', prodSand, data.campaignInfo, data.campaignInfo, {'nonInteraction': 1});
            } else {
                var tr = ga.getAll(),
                    trCnt = tr.length,
                    gtmName;

                while (trCnt --) {
                    gtmName = tr[trCnt].get('name');

                    if ((/^gtm/).test(gtmName)) {
                        namespace = tr[trCnt].get('name') + ".";
                    }
                }

                ga(namespace + 'set', dimensionName + dimensionIndex, data.campaignInfo);
                ga(namespace + 'send', 'event', prodSand, data.campaignInfo, data.campaignInfo, {'nonInteraction': 1});
            }

            if (typeof data.callback === 'function') data.callback();
            return true;
        }
    }
);