mmcore.IntegrationFactory.initialize('Google Universal', {
    campaign: 'Campaign Name',
    account: 'Account', // UA-123456-78, optional parameter, only to be used if requested by client/client services team, otherwise please delete this line.
    dimensionIndex: 'Dimension Index',
    redirect: false,
    persist: false,
    callback: function () {}
});